﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Core.Model
{
    public class Foods: BasePage
    {
        public int ID { get; set; }
        //public string GudID { get; set; } = Guid.NewGuid().ToString();
        public Guid GudID { get; set; }
        public string Baslik { get; set; }
        public string Konum { get; set; }
        public DateTime BaslangicZamani { get; set; }
        public string Saat { get; set; }
        public string Yemekler { get; set; }


    }
}
