﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Core.Model
{
   public  class Files:BasePage
    {
        public int ID { get; set; }
        //public string  FileGuid  { get; set; }
        public Guid  FileGuid  { get; set; }
        public string ParentFileID { get; set; }
        public string  FileName { get; set; }
        public string Description { get; set; }
        public string DisplayFileName { get; set; }
        public string  FilePath { get; set; }
        public string UrlPath { get; set; }
        public string KategoriName { get; set; }
    }
}
