﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Core.Model
{
   public  class Material_Check :BasePage
    {
        public int ID { get; set; }
        //public string PGuID { get; set; }
        public Guid PGuID { get; set; }
        //public string MGuID { get; set; }
        public Guid MGuID { get; set; }
        public string UserEmail { get; set; }
        public string Title { get; set; }
        public string Users { get; set; }
        public string  Piece { get; set; }
        public string OrderNo { get; set; }
        public string BasketStatus { get; set; }

    }
}
