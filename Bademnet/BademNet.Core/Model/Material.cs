﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Core.Model
{
    public class Material : BasePage
    {
        public int ID { get; set; }
        //public string GuID{ get; set; }
        public Guid GuID{ get; set; }
        public string Name { get; set; }
        public string Context { get; set; }
    }
}
