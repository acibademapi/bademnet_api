﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Core.Model
{
    public class Documents :BasePage
    {
         public int ID { get; set; }
        //public string DocumentGuID { get; set; }
        public Guid DocumentGuID { get; set; }
        public string DocumentName { get; set; }
        public string DocumentDisplayName { get; set; }
        public  string FileGuID { get; set; }
        public string Users { get; set; }
    }
}
