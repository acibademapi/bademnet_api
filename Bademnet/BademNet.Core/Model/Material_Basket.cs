﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Core.Model
{
    public class Material_Basket : BasePage
    {
        public int ID { get; set; }
        //public string MGUID { get; set; }
        public Guid MGUID { get; set; }
        public string Piece { get; set; }
        public string Users {get; set;}

    }
}
