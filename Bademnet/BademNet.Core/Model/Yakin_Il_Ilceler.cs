﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Core.Model
{
   public  class Yakin_Il_Ilceler
    {
        public int ID { get; set; }
        public int il_kodu { get; set; }
        public string il_adi { get; set; }
        public int ilce_kodu { get; set; }
        public  string ilce_adi { get; set; }
    }
}
