﻿using BademNet.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Core
{
   public  class TelefonRehber : BasePage
    {
        public int ID { get; set; }
        //public string GuID { get; set; }
        public Guid GuID { get; set; }
        public string  TellefonNumarasi { get; set; }
        public string Departman { get; set; }
        public string AdSoyad { get; set; }
        public string DahiliNumber { get; set; }
    }
}
