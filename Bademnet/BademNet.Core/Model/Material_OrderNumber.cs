﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Core.Model
{
   public  class Material_OrderNumber :BasePage
    {
        public int ID { get; set; }
        public string OrderNo { get; set; }
        public string  UserEmail { get; set; }
        public string User { get; set; }
        public string  Title { get; set; }
        public string OrderStatus { get; set; }
        public string Departman { get; set; }
    }
}
