﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Core.Model
{
    public class Kargo:BasePage
    {
        public int ID { get; set; }
        //public string GuID { get; set; } = Guid.NewGuid().ToString();
        public Guid GuID { get; set; } 
        public string KargoDurumu { get; set; }
        public string GonderenFirma { get; set; }
        public string Alici { get; set; }
        public string EvrakDurumu { get; set; }
        public string  RafDurumu { get; set; }
        public string  Aciklama { get; set; }
        public string TeslimNo { get; set; }
        public string  TeslimDurumu { get; set; }
        
    }
}
