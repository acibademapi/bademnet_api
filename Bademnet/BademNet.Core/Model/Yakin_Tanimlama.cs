﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Core.Model
{
    public class Yakin_Tanimlama: BasePage
    {
        public int ID { get; set; }
        public string Date { get; set; }
        public string EmployeeID { get; set; }
        public string CreatedBy { get; set; }
        public string YakinDerece { get; set; }
        public string YakinAltDerece { get; set; }
        public string YakinDereceAciklama { get; set; }
        public string TCKimlikNo { get; set; }
        public string YakinTCKimlikNo { get; set; }
        public string Ad { get; set; }
        public string SoyAd { get; set; }
        public string Cinsiyet { get; set; }
        public string  DogumTarihi { get; set; }
        public string EvTel { get; set; }
        public string IsTel { get; set; }
        public string CepTel { get; set; }
        //public string CreatedDate { get; set; }
        //public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Ulke { get; set; }
        public string il { get; set; }
        public string ilce { get; set; }
        public string Status { get; set; }
        public string Explanation { get; set; }
        //public string ISDelete { get; set; }
        public string DataField1 { get; set; }        
        public string DataField2 { get; set; }
        public string DataField3 { get; set; }
        public string DataField4 { get; set; }
        public string DataField5 { get; set; }


    }
}
