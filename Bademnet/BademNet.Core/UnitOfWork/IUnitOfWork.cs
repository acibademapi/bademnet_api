﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BademNet.Core.UnitOfWork
{
    public interface IUnitOfWork
    {
        Task CommitAsync();
        void Commit();
    }
}
