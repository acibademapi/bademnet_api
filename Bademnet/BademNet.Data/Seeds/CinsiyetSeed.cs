﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Seeds
{
   public  class CinsiyetSeed
    {
        private readonly int[] _ids;
        public CinsiyetSeed(int[] ids)
        {
            _ids = ids;
        }
        public void configure(EntityTypeBuilder<CinsiyetSeed> builder)
        {
            builder.HasData(
                        new Yakin_Cinsiyet { ID = 1,  Cinsiyet ="Erkek"},
                         new Yakin_Cinsiyet { ID = 2, Cinsiyet ="Kız" }
                        );
        }
        
    }
}
