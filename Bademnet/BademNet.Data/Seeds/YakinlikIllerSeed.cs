﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Seeds
{
    class YakinlikIllerSeed
    {
         private readonly int[] _ids;
        public YakinlikIllerSeed(int[] ids)
        {
            _ids = ids;
        }
        public void configure(EntityTypeBuilder<YakinlikIllerSeed> builder)
        {
            builder.HasData(
                    new Yakin_Iller { ID = 1, Plaka = 1, Il_Ad = "Adana" },
                    new Yakin_Iller { ID = 2, Plaka = 2, Il_Ad = "Adıyaman" },
                    new Yakin_Iller { ID = 3, Plaka = 3, Il_Ad = "Afyonkarahisar" },
                    new Yakin_Iller { ID = 4, Plaka = 4, Il_Ad = "Ağrı" },
                    new Yakin_Iller { ID = 5, Plaka = 5, Il_Ad = "Amasya" },
                    new Yakin_Iller { ID = 6, Plaka = 6, Il_Ad = "Ankara" },
                    new Yakin_Iller { ID = 7, Plaka = 7, Il_Ad = "Antalya" },
                    new Yakin_Iller { ID = 8, Plaka = 8, Il_Ad = "Artvin" },
                    new Yakin_Iller { ID = 9, Plaka = 9, Il_Ad = "Aydın" },
                    new Yakin_Iller { ID = 10, Plaka = 10, Il_Ad = "Balıkesir" },
                    new Yakin_Iller { ID = 11, Plaka = 11, Il_Ad = "Bilecik" },
                    new Yakin_Iller { ID = 12, Plaka = 12, Il_Ad = "Bingöl" },
                    new Yakin_Iller { ID = 13, Plaka = 13, Il_Ad = "Bitlis" },
                    new Yakin_Iller { ID = 14, Plaka = 14, Il_Ad = "Bolu" },
                    new Yakin_Iller { ID = 15, Plaka = 15, Il_Ad = "Burdur" },
                    new Yakin_Iller { ID = 16, Plaka = 16, Il_Ad = "Bursa" },
                    new Yakin_Iller { ID = 17, Plaka = 17, Il_Ad = "Çanakkale" },
                    new Yakin_Iller { ID = 18, Plaka = 18, Il_Ad = "Çankırı" },
                    new Yakin_Iller { ID = 19, Plaka = 19, Il_Ad = "Çorum" },
                    new Yakin_Iller { ID = 20, Plaka = 20, Il_Ad = "Denizli" },
                    new Yakin_Iller { ID = 21, Plaka = 21, Il_Ad = "Diyarbakır" },
                    new Yakin_Iller { ID = 22, Plaka = 22, Il_Ad = "Edirne" },
                    new Yakin_Iller { ID = 23, Plaka = 23, Il_Ad = "Elazığ" },
                    new Yakin_Iller { ID = 24, Plaka = 24, Il_Ad = "Erzincan" },
                    new Yakin_Iller { ID = 25, Plaka = 25, Il_Ad = "Erzurum" },
                    new Yakin_Iller { ID = 26, Plaka = 26, Il_Ad = "Eskişehir" },
                    new Yakin_Iller { ID = 27, Plaka = 27, Il_Ad = "Gaziantep" },
                    new Yakin_Iller { ID = 28, Plaka = 28, Il_Ad = "Giresun" },
                    new Yakin_Iller { ID = 29, Plaka = 29, Il_Ad = "Gümüşhane" },
                    new Yakin_Iller { ID = 30, Plaka = 30, Il_Ad = "Hakkâri" },
                    new Yakin_Iller { ID = 31, Plaka = 31, Il_Ad = "Hatay" },
                    new Yakin_Iller { ID = 32, Plaka = 32, Il_Ad = "Isparta" },
                    new Yakin_Iller { ID = 33, Plaka = 33, Il_Ad = "Mersin" },
                    new Yakin_Iller { ID = 34, Plaka = 34, Il_Ad = "İstanbul" },
                    new Yakin_Iller { ID = 35, Plaka = 35, Il_Ad = "İzmir" },
                    new Yakin_Iller { ID = 36, Plaka = 36, Il_Ad = "Kars" },
                    new Yakin_Iller { ID = 37, Plaka = 37, Il_Ad = "Kastamonu" },
                    new Yakin_Iller { ID = 38, Plaka = 38, Il_Ad = "Kayseri" },
                    new Yakin_Iller { ID = 39, Plaka = 39, Il_Ad = "Kırklareli" },
                    new Yakin_Iller { ID = 40, Plaka = 40, Il_Ad = "Kırşehir" },
                    new Yakin_Iller { ID = 41, Plaka = 41, Il_Ad = "Kocaeli" },
                    new Yakin_Iller { ID = 42, Plaka = 42, Il_Ad = "Konya" },
                    new Yakin_Iller { ID = 43, Plaka = 43, Il_Ad = "Kütahya" },
                    new Yakin_Iller { ID = 44, Plaka = 44, Il_Ad = "Malatya" },
                    new Yakin_Iller { ID = 45, Plaka = 45, Il_Ad = "Manisa" },
                    new Yakin_Iller { ID = 46, Plaka = 46, Il_Ad = "Kahramanmaraş" },
                    new Yakin_Iller { ID = 47, Plaka = 47, Il_Ad = "Mardin" },
                    new Yakin_Iller { ID = 48, Plaka = 48, Il_Ad = "Muğla" },
                    new Yakin_Iller { ID = 49, Plaka = 49, Il_Ad = "Muş" },
                    new Yakin_Iller { ID = 50, Plaka = 50, Il_Ad = "Nevşehir" },
                    new Yakin_Iller { ID = 51, Plaka = 51, Il_Ad = "Niğde" },
                    new Yakin_Iller { ID = 52, Plaka = 52, Il_Ad = "Ordu" },
                    new Yakin_Iller { ID = 53, Plaka = 53, Il_Ad = "Rize" },
                    new Yakin_Iller { ID = 54, Plaka = 54, Il_Ad = "Sakarya" },
                    new Yakin_Iller { ID = 55, Plaka = 55, Il_Ad = "Samsun" },
                    new Yakin_Iller { ID = 56, Plaka = 56, Il_Ad = "Siirt" },
                    new Yakin_Iller { ID = 57, Plaka = 57, Il_Ad = "Sinop" },
                    new Yakin_Iller { ID = 58, Plaka = 58, Il_Ad = "Sivas" },
                    new Yakin_Iller { ID = 59, Plaka = 59, Il_Ad = "Tekirdağ" },
                    new Yakin_Iller { ID = 60, Plaka = 60, Il_Ad = "Tokat" },
                    new Yakin_Iller { ID = 61, Plaka = 61, Il_Ad = "Trabzon" },
                    new Yakin_Iller { ID = 62, Plaka = 62, Il_Ad = "Tunceli" },
                    new Yakin_Iller { ID = 63, Plaka = 63, Il_Ad = "Şanlıurfa" },
                    new Yakin_Iller { ID = 64, Plaka = 64, Il_Ad = "Uşak" },
                    new Yakin_Iller { ID = 65, Plaka = 65, Il_Ad = "Van" },
                    new Yakin_Iller { ID = 66, Plaka = 66, Il_Ad = "Yozgat" },
                    new Yakin_Iller { ID = 67, Plaka = 67, Il_Ad = "Zonguldak" },
                    new Yakin_Iller { ID = 68, Plaka = 68, Il_Ad = "Aksaray" },
                    new Yakin_Iller { ID = 69, Plaka = 69, Il_Ad = "Bayburt" },
                    new Yakin_Iller { ID = 70, Plaka = 70, Il_Ad = "Karaman" },
                    new Yakin_Iller { ID = 71, Plaka = 71, Il_Ad = "Kırıkkale" },
                    new Yakin_Iller { ID = 72, Plaka = 72, Il_Ad = "Batman" },
                    new Yakin_Iller { ID = 73, Plaka = 73, Il_Ad = "Şırnak" },
                    new Yakin_Iller { ID = 74, Plaka = 74, Il_Ad = "Bartın" },
                    new Yakin_Iller { ID = 75, Plaka = 75, Il_Ad = "Ardahan" },
                    new Yakin_Iller { ID = 76, Plaka = 76, Il_Ad = "Iğdır" },
                    new Yakin_Iller { ID = 77, Plaka = 77, Il_Ad = "Yalova" },
                    new Yakin_Iller { ID = 78, Plaka = 78, Il_Ad = "Karabük" },
                    new Yakin_Iller { ID = 79, Plaka = 79, Il_Ad = "Kilis" },
                    new Yakin_Iller { ID = 80, Plaka = 80, Il_Ad = "Osmaniye" },
                    new Yakin_Iller { ID = 81, Plaka = 81, Il_Ad = "Düzce" }
                    );
        }

    }
}
