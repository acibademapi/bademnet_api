﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Seeds
{
    class YakinlikListeSeed
    {
        private readonly int[] _ids;
        public YakinlikListeSeed(int[] ids)
        {
            _ids = ids;
        }
        public void configure(EntityTypeBuilder<YakinlikListeSeed> builder)
        {
            builder.HasData(
                new YakinlikListe { DERECE = 1, ALT_DERECE = 1, DERECE_ACIKLAMA = "Es", AKTIF = 1 },
             new YakinlikListe { DERECE = 1, ALT_DERECE = 3, DERECE_ACIKLAMA = "Anne", AKTIF = 1 },
             new YakinlikListe { DERECE = 1, ALT_DERECE = 4, DERECE_ACIKLAMA = "Baba", AKTIF = 1 },
             new YakinlikListe { DERECE = 1, ALT_DERECE = 5, DERECE_ACIKLAMA = "Çocuk", AKTIF = 1 },
             new YakinlikListe { DERECE = 1, ALT_DERECE = 6, DERECE_ACIKLAMA = "Kayinvalide", AKTIF = 1 },
             new YakinlikListe { DERECE = 1, ALT_DERECE = 7, DERECE_ACIKLAMA = "Kayinpeder", AKTIF = 1 },
             new YakinlikListe { DERECE = 2, ALT_DERECE = 9, DERECE_ACIKLAMA = "Kardes", AKTIF = 1 },
             new YakinlikListe { DERECE = 2, ALT_DERECE = 10, DERECE_ACIKLAMA = "Torun", AKTIF = 1 },
             new YakinlikListe { DERECE = 2, ALT_DERECE = 11, DERECE_ACIKLAMA = "Büyükanne", AKTIF = 1 },
             new YakinlikListe { DERECE = 2, ALT_DERECE = 12, DERECE_ACIKLAMA = "Büyükbaba", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 13, DERECE_ACIKLAMA = "Esin Kardesi", AKTIF = 1 },
             new YakinlikListe { DERECE = 2, ALT_DERECE = 14, DERECE_ACIKLAMA = "Esin Büyükannesi", AKTIF = 1 },
             new YakinlikListe { DERECE = 2, ALT_DERECE = 15, DERECE_ACIKLAMA = "Esin Büyükbabasi", AKTIF = 1 },
             new YakinlikListe { DERECE = 3, ALT_DERECE = 17, DERECE_ACIKLAMA = "Kardes Çocugu", AKTIF = 1 },
             new YakinlikListe { DERECE = 3, ALT_DERECE = 18, DERECE_ACIKLAMA = "Amca", AKTIF = 1 },
             new YakinlikListe { DERECE = 3, ALT_DERECE = 19, DERECE_ACIKLAMA = "Dayi", AKTIF = 1 },
             new YakinlikListe { DERECE = 3, ALT_DERECE = 20, DERECE_ACIKLAMA = "Hala", AKTIF = 1 },
             new YakinlikListe { DERECE = 3, ALT_DERECE = 21, DERECE_ACIKLAMA = "Teyze", AKTIF = 1 },
             new YakinlikListe { DERECE = 3, ALT_DERECE = 22, DERECE_ACIKLAMA = "Esin Kardesinin Çocugu", AKTIF = 1 },
             new YakinlikListe { DERECE = 3, ALT_DERECE = 23, DERECE_ACIKLAMA = "Esin Amcasi", AKTIF = 1 },
             new YakinlikListe { DERECE = 3, ALT_DERECE = 24, DERECE_ACIKLAMA = "Esin Dayisi", AKTIF = 1 },
             new YakinlikListe { DERECE = 3, ALT_DERECE = 25, DERECE_ACIKLAMA = "Esin Halasi", AKTIF = 1 },
             new YakinlikListe { DERECE = 3, ALT_DERECE = 26, DERECE_ACIKLAMA = "Esin Teyzesi", AKTIF = 1 },
             new YakinlikListe { DERECE = 5, ALT_DERECE = 28, DERECE_ACIKLAMA = "Diger Tanidiklar", AKTIF = 0 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 29, DERECE_ACIKLAMA = "Kuzenler (Teyze, Hala, Dayi ve Amca çocuklari)", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 30, DERECE_ACIKLAMA = "Esin Kuzenleri (Esin teyzesinin, halasinin, dayisinin ve amcasinin çocuklari)", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 31, DERECE_ACIKLAMA = "Eniste (Kiz kardesin, hala ve teyzenin esi)", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 32, DERECE_ACIKLAMA = "Esin Enistesi (Esin kiz kardesinin, halasinin ve teyzesinin esi)", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 33, DERECE_ACIKLAMA = "Yenge (Erkek kardesin, dayi ve amcanin esi)", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 34, DERECE_ACIKLAMA = "Esin Yengesi (Esin erkek kardesinin, dayi ve amcasinin esi)", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 35, DERECE_ACIKLAMA = "Gelin (Erkek çocugun esi)", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 36, DERECE_ACIKLAMA = "Damat (Kiz çocugunun esi)", AKTIF = 1 },
             new YakinlikListe { DERECE = 1, ALT_DERECE = 37, DERECE_ACIKLAMA = "Kendim", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 38, DERECE_ACIKLAMA = "Kardes Torunu", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 39, DERECE_ACIKLAMA = "Esinin Kardesinin Torunu", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 40, DERECE_ACIKLAMA = "Kuzenin Esi", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 41, DERECE_ACIKLAMA = "Kuzen Çocuklari", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 42, DERECE_ACIKLAMA = "Esin Kuzen Çocuklari", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 43, DERECE_ACIKLAMA = "Annenin amca, hala, dayi veya teyzesi", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 44, DERECE_ACIKLAMA = "Babanin amca, hala, dayi veya teyzesi", AKTIF = 1 },
             new YakinlikListe { DERECE = 4, ALT_DERECE = 45, DERECE_ACIKLAMA = "Esin annesinin amca, hala, dayi veya teyzesi", AKTIF = 1 },
            new YakinlikListe { DERECE = 4, ALT_DERECE = 46, DERECE_ACIKLAMA = "Esin babasinin amca, hala, dayi veya teyzesi", AKTIF = 1 },
            new YakinlikListe { DERECE = 4, ALT_DERECE = 47, DERECE_ACIKLAMA = "Kardes çocugunun esi", AKTIF = 1 },
            new YakinlikListe { DERECE = 1, ALT_DERECE = 1, DERECE_ACIKLAMA = "Es", AKTIF = 1 });

        }
    }
}
