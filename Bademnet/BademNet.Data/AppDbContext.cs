﻿using BademNet.Core;
using BademNet.Core.Model;
using BademNet.Data.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data
{
    public class AppDbContext:DbContext

    {
        public AppDbContext(DbContextOptions<AppDbContext> options):base(options)
        {

        }
        public DbSet<Foods> Foods { get; set; }
        public DbSet<Kargo> Kargos { get; set; }       
        public DbSet<Documents> Documents { get; set; }
        public DbSet<Files> Files { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<Material_Basket> Material_Baskets { get; set; }
        public DbSet<Material_Check> Material_Checks { get; set; }
        public DbSet<TelefonRehber> TelefonRehbers { get; set; }
        public DbSet<Yakin_Cinsiyet> Yakın_Cinsiyets { get; set; }
        public DbSet<Yakin_Il_Ilceler> Yakin_Il_Ilcelers { get; set; }
        public DbSet<Yakin_Iller> Yakin_Illers { get; set; }
        public DbSet<Yakin_Tanimlama> Yakin_Tanimlamas { get; set; }
        public DbSet<YakinlikListe> YakinlikListes { get; set; }
        public DbSet<YakinlikUlke> YakinlikUlke { get; set; }
        public DbSet<Yakin_Cinsiyet> Yakin_Cinsiyet { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
             modelBuilder.ApplyConfiguration(new FoodConfiguration());
             modelBuilder.ApplyConfiguration(new YakinlikUlkeConfiguration());

        }





    }

  
}
