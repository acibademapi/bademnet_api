﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Configurations
{
    public class Material_CheckConfiguration : IEntityTypeConfiguration<Material_Check>
    {
        public void Configure(EntityTypeBuilder<Material_Check> builder)
        {
            // throw new NotImplementedException();
            builder.HasKey(x=>x.ID);
            builder.Property(x => x.ID).UseIdentityColumn().IsRequired();
            builder.Property(x => x.Title).IsRequired();
            builder.Property(x => x.Users).IsRequired();
            builder.Property(x => x.Piece).IsRequired();
            builder.Property(x=>x.UserEmail).IsRequired();
        }
    }
}
