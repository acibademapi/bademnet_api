﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Configurations
{
    public class MaterialConfiguration : IEntityTypeConfiguration<Material>
    {
        public void Configure(EntityTypeBuilder<Material> builder)
        {
            //throw new NotImplementedException();
            builder.HasKey(x => x.ID);
            builder.Property(x => x.ID).UseIdentityColumn().IsRequired();
            builder.Property(x => x.Name).IsRequired();
        }
    }
}
