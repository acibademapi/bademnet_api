﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Configurations
{
    class Material_OrderNumberConfiguration : IEntityTypeConfiguration<Material_OrderNumber>
    {
        public void Configure(EntityTypeBuilder<Material_OrderNumber> builder)
        {
            //throw new NotImplementedException();
            builder.HasKey(x => x.ID);
            builder.Property(x => x.ID).UseIdentityColumn().IsRequired();
            builder.Property(x => x.Title).IsRequired();
            builder.Property(x => x.Departman).IsRequired();
            builder.Property(x => x.OrderNo).IsRequired();
            builder.Property(x => x.User).IsRequired();
            builder.Property(x => x.UserEmail).IsRequired();
            builder.Property(x => x.OrderStatus).IsRequired();
        }
    }
}
