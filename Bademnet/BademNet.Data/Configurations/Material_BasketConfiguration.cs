﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Configurations
{
    class Material_BasketConfiguration : IEntityTypeConfiguration<Material_Basket>
    {
      

        public void Configure(EntityTypeBuilder<Material_Basket> builder)
        {
            // throw new NotImplementedException();
            builder.HasKey(x => x.ID);
            builder.Property(x => x.ID).UseIdentityColumn().IsRequired();
            builder.Property(x => x.Piece).IsRequired();
            builder.Property(x => x.Users).IsRequired();
        }
    }
}
