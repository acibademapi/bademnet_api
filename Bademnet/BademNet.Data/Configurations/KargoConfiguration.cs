﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Configurations
{
    public class KargoConfiguration : IEntityTypeConfiguration<Kargo>
    {
        public void Configure(EntityTypeBuilder<Kargo> builder)
        {
            //throw new NotImplementedException();
            builder.HasKey(x => x.ID);
            builder.Property(x => x.ID).IsRequired().UseIdentityColumn();
            builder.Property(x => x.Alici).IsRequired();
            builder.Property(x => x.GonderenFirma).IsRequired();
            builder.Property(x => x.EvrakDurumu).IsRequired();
        }
    }
}
