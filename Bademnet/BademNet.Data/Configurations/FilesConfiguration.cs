﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Core.Model
{
    public class FilesConfiguration : IEntityTypeConfiguration<Files>
    {
        public void Configure(EntityTypeBuilder<Files> builder)
        {
            //throw new NotImplementedException();
            builder.HasKey(x => x.ID);
            builder.Property(x => x.ID).IsRequired().UseIdentityColumn().IsRequired();
            builder.Property(x => x.FileName).IsRequired();
        }
    }
}
