﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Configurations
{
    public class FoodConfiguration : IEntityTypeConfiguration<Foods>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Foods> builder)
        {
            builder.HasKey(x => x.ID);
            builder.Property(x => x.ID).UseIdentityColumn();
            builder.Property(x => x.Konum).IsRequired().HasMaxLength(250);
        }
    }
}
