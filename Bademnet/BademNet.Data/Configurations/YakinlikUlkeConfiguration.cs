﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Configurations
{
    public class YakinlikUlkeConfiguration : IEntityTypeConfiguration<YakinlikUlke>
    {
        public void Configure(EntityTypeBuilder<YakinlikUlke> builder)
        {
            // throw new NotImplementedException();
            builder.HasKey(x => x.ID);
            builder.Property(x => x.ID).UseIdentityColumn();
            builder.Property(x => x.Ulke).IsRequired();
        }
    }
}
