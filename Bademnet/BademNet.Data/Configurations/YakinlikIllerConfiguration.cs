﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Configurations
{
    class YakinlikIllerConfiguration : IEntityTypeConfiguration<Yakin_Iller>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Yakin_Iller> builder)
        {
            builder.HasKey(x => x.ID);
            builder.Property(x => x.ID).UseIdentityColumn();
            builder.Property(x => x.ID).IsRequired();
            builder.Property(x => x.Plaka).IsRequired();
            builder.Property(x => x.Plaka).IsRequired();
        }
    }
}
