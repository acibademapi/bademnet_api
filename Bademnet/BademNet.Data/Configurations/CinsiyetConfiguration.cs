﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Configurations
{
    public class CinsiyetConfiguration : IEntityTypeConfiguration<Yakin_Cinsiyet>
    {
        public void Configure(EntityTypeBuilder<Yakin_Cinsiyet> builder)
        {
            builder.HasKey(x => x.ID);
            builder.Property(x => x.ID).IsRequired().UseIdentityColumn();
            builder.Property(x => x.Cinsiyet).IsRequired();

        }
    }
}
