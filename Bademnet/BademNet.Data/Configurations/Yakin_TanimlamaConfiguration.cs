﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Configurations
{
    public class Yakin_TanimlamaConfiguration : IEntityTypeConfiguration<Yakin_Tanimlama>
    {
        public void Configure(EntityTypeBuilder<Yakin_Tanimlama> builder)
        {
            builder.HasKey(x => x.ID);
            builder.Property(x => x.ID).IsRequired();
            builder.Property(x => x.ID).UseIdentityColumn();
            builder.Property(x => x.EmployeeID).IsRequired();
            builder.Property(x => x.TCKimlikNo).IsRequired();
            builder.Property(x => x.Ad).IsRequired();
            builder.Property(x=>x.SoyAd).IsRequired();
            builder.Property(x => x.CepTel).IsRequired();
            builder.Property(x=>x.Cinsiyet).IsRequired();
            builder.Property(x => x.YakinTCKimlikNo).IsRequired();
            builder.Property(x => x.DogumTarihi).IsRequired();
        }
    }
}
