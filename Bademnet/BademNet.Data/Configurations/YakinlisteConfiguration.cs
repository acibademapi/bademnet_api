﻿using BademNet.Core.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Configurations
{
    public class YakinlisteConfiguration : IEntityTypeConfiguration<YakinlikListe>
    {
        public void Configure(EntityTypeBuilder<YakinlikListe> builder)
        {
            builder.HasKey(x => x.ID);
            builder.Property(x => x.ID).UseIdentityColumn();
            builder.Property(x => x.DERECE).IsRequired();
            builder.Property(x => x.ALT_DERECE).IsRequired();
            builder.Property(x => x.DERECE_ACIKLAMA).IsRequired();
            builder.Property(x => x.GuID).IsRequired();
        }
    }
}
