﻿using BademNet.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BademNet.Data.Configurations
{
    class TelefonRehberConfiguration : IEntityTypeConfiguration<TelefonRehber>
    {
        public void Configure(EntityTypeBuilder<TelefonRehber> builder)
        {
            builder.HasKey(x => x.ID);
            builder.Property(x => x.ID).UseIdentityColumn().IsRequired();
            builder.Property(x => x.AdSoyad).IsRequired();
            builder.Property(x => x.DahiliNumber).IsRequired();
            builder.Property(x => x.Departman).IsRequired();

        }
    }
}
